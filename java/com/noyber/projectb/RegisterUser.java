package com.noyber.projectb;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUser;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUserAttributes;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUserCodeDeliveryDetails;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.handlers.SignUpHandler;

public class RegisterUser extends Activity {

    private EditText password;
    private EditText givenName;
    private EditText familyName;
    private EditText email;
    private EditText username;

    private Button signUp;
    private ProgressDialog waitDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_user);

        password = (EditText) findViewById(R.id.passSignUp);
        givenName = (EditText) findViewById(R.id.givenName);
        familyName = (EditText) findViewById(R.id.familyName);
        email = (EditText) findViewById(R.id.email);
        username = (EditText) findViewById(R.id.usernameSignUp);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            // get back to main screen
            String value = extras.getString("TODO");
            if (value.equals("exit")) {
                onBackPressed();
            }
        }

        TextView main_title = (TextView) findViewById(R.id.registerTitle);
        main_title.setText("REGISTER NEW USER");

        signUp = (Button) findViewById(R.id.signUpBtn);
        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Read user data and register
                CognitoUserAttributes userAttributes = new CognitoUserAttributes();

                String usernameInput = username.getText().toString();
                if (usernameInput == null || usernameInput.isEmpty()) {
                    Toast.makeText(getApplicationContext(), "Username cannot be empty.", Toast.LENGTH_SHORT).show();
                    return;
                }
                userAttributes.addAttribute("preferred_username", usernameInput);

                String userPassword = password.getText().toString();
                if (userPassword == null || userPassword.isEmpty()) {
                    Toast.makeText(getApplicationContext(), "Password cannot be empty.", Toast.LENGTH_SHORT).show();
                    return;
                }

                String givenNameInput = givenName.getText().toString();
                if (givenNameInput == null || givenNameInput.isEmpty()) {
                    Toast.makeText(getApplicationContext(), "Given name cannot be empty.", Toast.LENGTH_SHORT).show();
                    return;
                }
                userAttributes.addAttribute("given_name", givenNameInput);

                String familyNameInput = familyName.getText().toString();
                if (familyNameInput == null || familyNameInput.isEmpty()) {
                    Toast.makeText(getApplicationContext(), "Family name cannot be empty.", Toast.LENGTH_SHORT).show();
                    return;
                }
                userAttributes.addAttribute("family_name", familyNameInput);

                String emailInput = email.getText().toString();
                if (emailInput == null || emailInput.isEmpty()) {
                    Toast.makeText(getApplicationContext(), "E-mail cannot be empty.", Toast.LENGTH_SHORT).show();
                    return;
                }
                userAttributes.addAttribute("email", emailInput);

                userAttributes.addAttribute("profile", "regular");

                showWaitDialog("Signing up...");

                UserSession.getPool().signUpInBackground(usernameInput, userPassword, userAttributes, null, signUpHandler);
            }
        });
    }

    SignUpHandler signUpHandler = new SignUpHandler() {
        @Override
        public void onSuccess(CognitoUser user, boolean signUpConfirmationState,
                              CognitoUserCodeDeliveryDetails cognitoUserCodeDeliveryDetails) {
            // Check signUpConfirmationState to see if the user is already confirmed
            closeWaitDialog();
            if (signUpConfirmationState) {
                // User is already confirmed
                Toast.makeText(getApplicationContext(), "Sign up successful!"+username+" has been Confirmed", Toast.LENGTH_SHORT).show();
            }
            else {
                // User is not confirmed
                confirmSignUp(cognitoUserCodeDeliveryDetails);
            }
        }

        @Override
        public void onFailure(Exception exception) {
            closeWaitDialog();
            Log.e("ERROR: ", exception.getMessage());
            Toast.makeText(getApplicationContext(), "Sign up faild.", Toast.LENGTH_SHORT).show();
        }
    };

    private void confirmSignUp(CognitoUserCodeDeliveryDetails cognitoUserCodeDeliveryDetails) {
        Intent intent = new Intent(this, SignUpConfirm.class);
        intent.putExtra("source","signup");
        intent.putExtra("name", username.getText().toString());
        intent.putExtra("destination", cognitoUserCodeDeliveryDetails.getDestination());
        intent.putExtra("deliveryMed", cognitoUserCodeDeliveryDetails.getDeliveryMedium());
        intent.putExtra("attribute", cognitoUserCodeDeliveryDetails.getAttributeName());
        startActivityForResult(intent, 10);
    }

    private void showWaitDialog(String message) {
        closeWaitDialog();
        waitDialog = new ProgressDialog(this);
        waitDialog.setTitle(message);
        waitDialog.show();
    }

    private void closeWaitDialog() {
        try {
            waitDialog.dismiss();
        }
        catch (Exception e) {
            //
        }
    }
}
