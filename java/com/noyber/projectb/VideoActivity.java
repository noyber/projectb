package com.noyber.projectb;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Environment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;
import android.widget.Toast;

import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.DynamoDBScanExpression;
import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.PaginatedScanList;
import com.amazonaws.services.s3.model.GetObjectRequest;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class VideoActivity extends AppCompatActivity {

    private AlertDialog userDialog;
    private TextView title;
    private TextView usersIn;
    private String name;
    private String location;
    private String ip;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_video);

        Intent intent = getIntent();
        name = intent.getStringExtra("name");
        ip = intent.getStringExtra("ip");
        location = intent.getStringExtra("location");

        title = (TextView) findViewById(R.id.videoTitle);
        title.setText(name.toUpperCase() + " STREAM");

        usersIn = (TextView) findViewById(R.id.textUsersIn);
        List<String> users = new ArrayList<>();
        final DynamoDBScanExpression scanExpression = new DynamoDBScanExpression();
        PaginatedScanList<ClientLocation> result = UserSession.getMapper().scan(ClientLocation.class, scanExpression);
        for(ClientLocation l : result){
            if(l.getPlace().equals(name))
                users.add(l.getUsername());
        }

        String usersString = "Users at location:\n| ";
        if(users.isEmpty()){
            usersIn.setText("No one is around at the moment.");
//            showDialogMessage("ERROR", "Streaming channel is not available right now.");
        } else {
            for(String user : users){
                usersString = usersString.concat(user + " | ");
            }
            usersIn.setText(usersString);
        }

        final WebView webView = (WebView) findViewById(R.id.webView);
        webView.setBackgroundColor(Color.TRANSPARENT);
        webView.getSettings().setJavaScriptEnabled(true);

        webView.setWebViewClient(new WebViewClient(){
            @Override
            public void onLoadResource(WebView view, String url) {
                super.onLoadResource(view, url);
                webView.setEnabled(false);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                webView.setEnabled(true);
            }

            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUr){
                super.onReceivedError(view, errorCode, description, failingUr);
                //Your code to do
                Toast.makeText(getApplicationContext(), "Stream is not available right now" , Toast.LENGTH_LONG).show();
                webView.loadUrl("about:blank");
                webView.setBackgroundResource(R.drawable.streamerrormessage);            }
        });
        webView.loadUrl("http://" + ip + ":9000/stream/video.mjpeg");
    }

    public void downloadVideo(View view) throws IOException {
        File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + "/" + name + ".mp4");
        UserSession.getS3Client().getObject(new GetObjectRequest("myrspbucket",name + ".mp4"), file);
    }

    private void showDialogMessage(String title, String body) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title).setMessage(body).setNeutralButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                try {
                    userDialog.dismiss();
                } catch (Exception e) {
                    // Log failure
                    Log.e(getClass().toString(),"Dialog dismiss failed");
                }
            }
        });
        userDialog = builder.create();
        userDialog.show();
    }
}
