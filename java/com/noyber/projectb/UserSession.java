package com.noyber.projectb;

import android.content.Context;
import android.util.Log;
import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUserDetails;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUserPool;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUserSession;
import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.DynamoDBMapper;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.kinesis.AmazonKinesisClient;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UserSession {

    // App settings
    private static List<String> attributeDisplaySeq;
    private static Map<String, String> signUpFieldsC2O;
    private static Map<String, String> signUpFieldsO2C;

    private static UserSession userSession;
    private static CognitoCachingCredentialsProvider currCredentialsProvider;
    private static CognitoUserPool userPool;
    private static String user;

    private static final String userPoolId = "us-east-1_AcF8roFaJ";
    private static final String clientId = "1j0tl6g4e8u49rico6ghgf7b5l";
    private static final String clientSecret = null;
    private static final Regions cognitoRegion = Regions.US_EAST_1;

    // User details from the service
    private static CognitoUserSession currSession;
    private static CognitoUserDetails userDetails;

    private static AmazonKinesisClient amazonKinesisClient;
    private static AmazonDynamoDBClient ddbClient;
    private static DynamoDBMapper mapper;
    private static AmazonS3 s3Client;

    public static void init(Context context) {
        if (userSession != null && userPool != null) {
            return;
        }

        if (userSession == null) {
            userSession = new UserSession();
        }

        if (userPool == null) {
            // Create a user pool with default ClientConfiguration
            userPool = new CognitoUserPool(context, userPoolId, clientId, clientSecret, cognitoRegion);
        }
    }

    public static AmazonDynamoDBClient getDdbClient() {
        return ddbClient;
    }

    public static DynamoDBMapper getMapper() {
        return mapper;
    }

    public static AmazonKinesisClient getAmazonKinesisClient() {
        return amazonKinesisClient;
    }

    public static AmazonS3 getS3Client() {
        return s3Client;
    }

    public static CognitoUserPool getPool() {
        return userPool;
    }

    public static void setCurrSession(CognitoUserSession session) {
        currSession = session;
    }

    public static void setCredentials(CognitoCachingCredentialsProvider credentialsProvider){
        currCredentialsProvider = credentialsProvider;
        amazonKinesisClient = new AmazonKinesisClient(credentialsProvider);
        ddbClient = new AmazonDynamoDBClient(credentialsProvider);
        mapper = new DynamoDBMapper(ddbClient);
        s3Client = new AmazonS3Client(credentialsProvider);
    }

    public static CognitoCachingCredentialsProvider getCredentials(){
        return currCredentialsProvider;
    }

    public static String getCurrUser() {
        return user;
    }

    public static void setUser(String newUser) {
        user = newUser;
    }

    public static void setUserDetails(CognitoUserDetails details) {
        userDetails = details;
    }

    public static String getUserAttribute(String attribute) {
        Map<String, String> attributes = userDetails.getAttributes().getAttributes();
        if(attributes.containsKey(attribute)){
            return attributes.get(attribute);
        }
        return "";
    }

    public static String formatException(Exception exception) {
        String formattedString = "Internal Error";
        Log.e("App Error",exception.toString());
        Log.getStackTraceString(exception);

        String temp = exception.getMessage();

        if(temp != null && temp.length() > 0) {
            formattedString = temp.split("\\(")[0];
            if(temp != null && temp.length() > 0) {
                return formattedString;
            }
        }

        return  formattedString;
    }
}
