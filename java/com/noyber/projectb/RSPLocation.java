package com.noyber.projectb;
import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.*;

@DynamoDBTable(tableName = "RSPLocation")
public class RSPLocation {
    private String location;
    private String name;
    private String ip;

    @DynamoDBAttribute(attributeName = "ip")
    public String getIp() {
        return ip;
    }

    @DynamoDBAttribute(attributeName = "location")
    public String getLocation() {
        return location;
    }

    @DynamoDBHashKey(attributeName = "name")
    public String getName() {
        return name;
    }

    public void setLocation(String newLocation) {
        location = newLocation;
    }

    public void setName(String newName) {
        name = newName;
    }

    public void setIp(String newIp) {
        ip = newIp;
    }
}
