package com.noyber.projectb;

import java.io.IOException;
import java.sql.Timestamp;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;


public class GeofenceEvent {
    private Timestamp timestamp;
    private String type;
    private String username;
    private String place;

    private final static ObjectMapper JSON = new ObjectMapper();
    static {
        JSON.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    public GeofenceEvent(String place, Timestamp timestamp, String type, String username) {
        this.place = place;
        this.timestamp = timestamp;
        this.type = type;
        this.username = username;
    }

    public String getType() {
        return type;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public String getUsername() {
        return username;
    }

    public String getPlace() {
        return place;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    public byte[] toJsonAsBytes() {
        try {
            return JSON.writeValueAsBytes(this);
        } catch (IOException e) {
            return null;
        }
    }

    @Override
    public String toString() {
        return username + " " + type + " " + place + ".";
    }
}
