package com.noyber.projectb;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.DynamoDBMapper;
import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.DynamoDBScanExpression;
import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.PaginatedScanList;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;

public class AdminActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);

        LinearLayout ll = (LinearLayout)findViewById(R.id.buttonlayout);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        final DynamoDBScanExpression scanExpression = new DynamoDBScanExpression();
        PaginatedScanList<RSPLocation> result = UserSession.getMapper().scan(RSPLocation.class, scanExpression);
        for(RSPLocation l : result){
            Button button = new Button(this);
            button.setText(l.getName());
            ll.addView(button, lp);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Button b = (Button) v;
                    Intent videoActivity = new Intent(getApplicationContext(), VideoActivity.class);
                    RSPLocation rspLocation = UserSession.getMapper().load(RSPLocation.class, b.getText());
                    videoActivity.putExtra("ip", rspLocation.getIp());
                    videoActivity.putExtra("location", rspLocation.getLocation());
                    videoActivity.putExtra("name", rspLocation.getName());
                    startActivity(videoActivity);
                }
            });
        }
    }
}
