from AWSIoTPythonSDK.MQTTLib import AWSIoTMQTTShadowClient
# from pif import get_public_ip
import boto3
import sys
import logging
import time
import getopt
import json
import picamera
import io
import threading
import signal
import os
import subprocess
import requests
import socket
import fcntl
import struct

NAME = "noys"
AWS_BUCKET = "myrspbucket"

class shadowCallbackContainer(threading.Thread):
        
	def __init__(self, deviceShadowInstance):
		self.deviceShadowInstance = deviceShadowInstance
		self.pro = None
		self.counter=0
	
        # Custom Shadow callback
        def customShadowCallback_Delta(self, payload, responseStatus, token):
                payloadDict = json.loads(payload)
                print("++++++++DELTA++++++++++")
                print("state: " + str(payloadDict["state"]))
                print("version: " + str(payloadDict["version"]))
                print("+++++++++++++++++++++++\n\n")

                print("Request to update the reported state...")
                deltaMessage = json.dumps(payloadDict["state"])
		newPayload = '{"state":{"reported":' + deltaMessage + '}}'
		self.deviceShadowInstance.shadowUpdate(newPayload, None, 5)
		print("Sent.")

                state = payloadDict["state"]
                if 'in' in state:
                        if self.counter==0:
                                cmd = 'ffmpeg -i http://10.0.0.7:9000/stream/video.mjpeg -f segment -segment_time 60 -segment_wrap 1 -segment_format mp4 -c:v libx264 -an -crf 20 /home/pi/Desktop/connect/out%0d.mp4'
                                # The os.setsid() is passed in the argument preexec_fn so
                                # it's run after the fork() and before  exec() to run the shell.
                                self.pro = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True, preexec_fn=os.setsid)
                        self.counter+=1
                        print("number of users in: {}".format(self.counter))
                elif 'out' in state:
                        self.counter-=1
                        print("number of users in: {}".format(self.counter))
                        if self.counter==0:
                                os.killpg(os.getpgid(self.pro.pid), signal.SIGTERM)  # Send the signal to all the process groups
                                self.pro.wait()
                                time.sleep(5)
                                # Upload the video file to S3
                                print("Done recording, uploading to S3")
                                file_path = "/home/pi/Desktop/connect/out0.mp4"
                                s3.meta.client.upload_file(file_path, AWS_BUCKET, NAME + ".mp4")
                                print("Done uploading.")


def get_ip_address(ifname):
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    return socket.inet_ntoa(fcntl.ioctl(
        s.fileno(),
        0x8915,  # SIOCGIFADDR
        struct.pack('256s', ifname[:15])
    )[20:24])


# Usage
usageInfo = """Usage:
Use certificate based mutual authentication:
python upload.py -e <endpoint> -r <rootCAFilePath> -c <certFilePath> -k <privateKeyFilePath>
Use MQTT over WebSocket:
python upload.py -e <endpoint> -r <rootCAFilePath> -w
Type "python upload.py -h" for available options.
"""
# Help info
helpInfo = """-e, --endpoint
	Your AWS IoT custom endpoint
-r, --rootCA
	Root CA file path
-c, --cert
	Certificate file path
-k, --key
	Private key file path
-w, --websocket
	Use MQTT over WebSocket
-h, --help
	Help information
"""

# Read in command-line parameters
useWebsocket = False
host = ""
rootCAPath = ""
certificatePath = ""
privateKeyPath = ""
try:
	opts, args = getopt.getopt(sys.argv[1:], "hwe:k:c:r:", ["help", "endpoint=", "key=","cert=","rootCA=", "websocket"])
	if len(opts) == 0:
		raise getopt.GetoptError("No input parameters!")
	for opt, arg in opts:
		if opt in ("-h", "--help"):
			print(helpInfo)
			exit(0)
		if opt in ("-e", "--endpoint"):
			host = arg
		if opt in ("-r", "--rootCA"):
			rootCAPath = arg
		if opt in ("-c", "--cert"):
			certificatePath = arg
		if opt in ("-k", "--key"):
			privateKeyPath = arg
		if opt in ("-w", "--websocket"):
			useWebsocket = True
except getopt.GetoptError:
	print(usageInfo)
	exit(1)

# Missing configuration notification
missingConfiguration = False
if not host:
	print("Missing '-e' or '--endpoint'")
	missingConfiguration = True
if not rootCAPath:
	print("Missing '-r' or '--rootCA'")
	missingConfiguration = True
if not useWebsocket:
	if not certificatePath:
		print("Missing '-c' or '--cert'")
		missingConfiguration = True
	if not privateKeyPath:
		print("Missing '-k' or '--key'")
		missingConfiguration = True
if missingConfiguration:
	exit(2)

# Configure logging
logger = logging.getLogger("AWSIoTPythonSDK.core")
logger.setLevel(logging.DEBUG)
streamHandler = logging.StreamHandler()
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
streamHandler.setFormatter(formatter)
logger.addHandler(streamHandler)

# Init AWSIoTMQTTShadowClient
myAWSIoTMQTTShadowClient = None
if useWebsocket:
	myAWSIoTMQTTShadowClient = AWSIoTMQTTShadowClient(NAME + "basicShadowDeltaListener", useWebsocket=True)
	myAWSIoTMQTTShadowClient.configureEndpoint(host, 443)
	myAWSIoTMQTTShadowClient.configureCredentials(rootCAPath)
else:
	myAWSIoTMQTTShadowClient = AWSIoTMQTTShadowClient(NAME + "basicShadowDeltaListener")
	myAWSIoTMQTTShadowClient.configureEndpoint(host, 8883)
	myAWSIoTMQTTShadowClient.configureCredentials(rootCAPath, privateKeyPath, certificatePath)

# AWSIoTMQTTShadowClient configuration
myAWSIoTMQTTShadowClient.configureAutoReconnectBackoffTime(1, 32, 20)
myAWSIoTMQTTShadowClient.configureConnectDisconnectTimeout(10)  # 10 sec
myAWSIoTMQTTShadowClient.configureMQTTOperationTimeout(5)  # 5 sec

# Connect to AWS IoT
myAWSIoTMQTTShadowClient.connect()

s3 = boto3.resource('s3')
dynamodb = boto3.resource('dynamodb')

# Update public IP of this thing
table = dynamodb.Table('RSPLocation')
table.update_item(
    Key={
        'name': NAME,
    },
    UpdateExpression='SET ip = :val1',
    ExpressionAttributeValues={
#        ':val1': get_public_ip()
        ':val1': get_ip_address('wlan0')
    }
)

# Create a deviceShadow with persistent subscription
myRSP = myAWSIoTMQTTShadowClient.createShadowHandlerWithName(NAME, True)
shadowCallbackContainer_myRSP = shadowCallbackContainer(myRSP)

# Listen on deltas
myRSP.shadowRegisterDeltaCallback(shadowCallbackContainer_myRSP.customShadowCallback_Delta)

# Loop forever
while True:
        pass
